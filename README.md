# Inuits DAMS Search API

This is the Inuits DAMS Search API repository for the Inuits DAMS project. See https://gitlab.inuits.io/customers/coghent/dams/coghent-dams-common for general information.

## Local setup

The common repository contains information about hw to easily run a local (developement) enviroment.

## Documentation

When the API is started, documentation about the endpoints is provided on:
* http://search-api.dams.localhost:8100/api/docs
