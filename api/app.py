import logging
import os
import secrets

from flask import Flask
from flask_restful import Api
from flask_swagger_ui import get_swaggerui_blueprint
from healthcheck import HealthCheck
from inuits_jwt_auth.authorization import JWTValidator, MyResourceProtector
from search_engine.search_manager import SearchManager

if os.getenv("SENTRY_ENABLED", False) in ["True", "true", True]:
    import sentry_sdk
    from sentry_sdk.integrations.flask import FlaskIntegration

    sentry_sdk.init(
        dsn=os.getenv("SENTRY_DSN"),
        integrations=[FlaskIntegration()],
        environment=os.getenv("NOMAD_NAMESPACE"),
    )

SWAGGER_URL = "/api/docs"  # URL for exposing Swagger UI (without trailing '/')
API_URL = (
    "/spec/dams-search-api.json"  # Our API url (can of course be a local resource)
)

swaggerui_blueprint = get_swaggerui_blueprint(SWAGGER_URL, API_URL)

app = Flask(__name__)
api = Api(app)
app.secret_key = os.getenv("SECRET_KEY", secrets.token_hex(16))

logging.basicConfig(
    format="%(asctime)s %(process)d,%(threadName)s %(filename)s:%(lineno)d [%(levelname)s] %(message)s",
    datefmt="%Y-%m-%d %H:%M:%S",
    level=logging.INFO,
)
logger = logging.getLogger(__name__)

require_oauth = MyResourceProtector(
    logger,
    os.getenv("REQUIRE_TOKEN", True) in ["True", "true", True],
)
validator = JWTValidator(
    logger,
    os.getenv("STATIC_ISSUER", False),
    os.getenv("STATIC_PUBLIC_KEY", False),
    os.getenv("REALMS", "").split(","),
    os.getenv("ROLE_PERMISSION_FILE", "role_permission.json"),
    os.getenv("SUPER_ADMIN_ROLE", "role_super_admin"),
    os.getenv("REMOTE_TOKEN_VALIDATION", False) in ["True", "true", True],
    os.getenv("REMOTE_PUBLIC_KEY", False),
)
require_oauth.register_token_validator(validator)

app.register_blueprint(swaggerui_blueprint)


@app.after_request
def add_header(response):
    response.headers["Access-Control-Allow-Origin"] = "*"
    return response


def search_engine_available():
    return True, SearchManager().get_search_engine().check_health()


health = HealthCheck()
if os.getenv("HEALTH_CHECK_EXTERNAL_SERVICES", True) in ["True", "true", True]:
    health.add_check(search_engine_available)
app.add_url_rule("/health", "healthcheck", view_func=lambda: health.run())

from resources.advanced_search import (
    AdvancedSearchEntities,
    AdvancedSearchEntitiesBySavedSearchId,
    AdvancedSearchMediafiles,
    AdvancedSearchMediafilesBySavedSearchId,
)
from resources.search import SearchCollection, SearchRaw, SearchRelations
from resources.spec import OpenAPISpec

api.add_resource(SearchCollection, "/search/collection")
api.add_resource(SearchRaw, "/search/raw")
api.add_resource(SearchRelations, "/search/relations")
api.add_resource(
    AdvancedSearchEntities,
    "/advanced-search",
    resource_class_kwargs={"search_engine": "arango"},
)
api.add_resource(
    AdvancedSearchEntitiesBySavedSearchId,
    "/advanced-search/<string:id>",
    resource_class_kwargs={"search_engine": "arango"},
)
api.add_resource(
    AdvancedSearchMediafiles,
    "/advanced-search/mediafiles",
    resource_class_kwargs={"search_engine": "arango"},
)
api.add_resource(
    AdvancedSearchMediafilesBySavedSearchId,
    "/advanced-search/mediafiles/<string:id>",
    resource_class_kwargs={"search_engine": "arango"},
)

api.add_resource(OpenAPISpec, "/spec/dams-search-api.json")

if __name__ == "__main__":
    app.run()
