import app

from resources.base_resource import BaseResource


class AdvancedSearchEntities(BaseResource):
    @app.require_oauth("search-advanced")
    def post(self):
        return self._execute_advanced_search("entities")


class AdvancedSearchEntitiesBySavedSearchId(BaseResource):
    @app.require_oauth("search-advanced")
    def post(self, id):
        return self._execute_advanced_search_with_saved_search(id, "entities")


class AdvancedSearchMediafiles(BaseResource):
    @app.require_oauth("search-advanced")
    def post(self):
        return self._execute_advanced_search("mediafiles")


class AdvancedSearchMediafilesBySavedSearchId(BaseResource):
    @app.require_oauth("search-advanced")
    def post(self, id):
        return self._execute_advanced_search_with_saved_search(id, "mediafiles")
