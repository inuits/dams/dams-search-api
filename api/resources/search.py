import app

from flask import request
from resources.base_resource import BaseResource


class SearchRaw(BaseResource):
    @app.require_oauth("search-raw")
    def get(self):
        query = request.args.get("query")
        skip = int(request.args.get("skip", 0))
        limit = int(request.args.get("limit", 20))
        order_by = request.args.get("order_by", None)
        ascending = request.args.get("asc", 1, int)
        return self.search_engine.search("raw", query, skip, limit)


class SearchCollection(BaseResource):
    @app.require_oauth("search-collection")
    def post(self):
        body = request.get_json()
        skip = int(request.args.get("skip", 0))
        limit = int(request.args.get("limit", 20))
        order_by = request.args.get("order_by", None)
        ascending = request.args.get("asc", 1, int)
        auth_token = None
        if self._only_own_items():
            auth_token = request.headers.get("Authorization")
        return self.search_engine.search("collection", body, skip, limit, auth_token)


class SearchRelations(BaseResource):
    @app.require_oauth("search-relations")
    def post(self):
        return self.search_engine.search_relations(request.get_json())
