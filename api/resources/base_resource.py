import app
import os
import requests

from flask import request
from flask_restful import abort, Resource
from search_engine.search_manager import SearchManager


class BaseResource(Resource):
    def __init__(self, search_engine=None):
        self.collection_api_url = os.getenv("COLLECTION_API_URL")
        self.headers = {"Authorization": f'Bearer {os.getenv("STATIC_JWT", "None")}'}
        if search_engine:
            try:
                self.search_engine = SearchManager().get_specific_search_engine(
                    search_engine
                )
            except KeyError:
                self.search_engine = None
                app.logger.error("Database does not exist")
        else:
            self.search_engine = SearchManager().get_search_engine()

    def _execute_advanced_search(self, collection="entities"):
        body = request.get_json()
        skip = int(request.args.get("skip", 0))
        limit = int(request.args.get("limit", 20))
        if not self.search_engine:
            abort(500, message="Failed to init search engine")
        self.validate_advanced_query_syntax(body)
        return self.search_engine.search("", body, skip, limit, collection)

    def _execute_advanced_search_with_saved_search(
        self, saved_search_id, collection="entities"
    ):
        skip = int(request.args.get("skip", 0))
        limit = int(request.args.get("limit", 20))
        response = requests.get(
            f"{self.collection_api_url}/saved_searches/{saved_search_id}",
            headers=self.headers,
        )
        if response.status_code == 404:
            abort(404, message=f"Saved search {saved_search_id} not found")
        elif response.status_code != 200:
            abort(400, message=f"Could not get saved search: {response.text.strip()}")
        return self.search_engine.search(
            "", response.json()["definition"], skip, limit, collection
        )

    def _only_own_items(self):
        return not app.require_oauth.check_permission("show-all")

    def validate_advanced_query_syntax(self, queries):
        for query in queries:
            if query["type"] == "MinMaxInput":
                if "min" not in query["value"] and "max" not in query["value"]:
                    abort(
                        400,
                        message="MinMaxfilter must specify min and/or max value, none are specified",
                    )
                if (
                    "min" in query["value"]
                    and "max" in query["value"]
                    and query["value"]["min"] > query["value"]["max"]
                ):
                    abort(
                        400,
                        message="Min-value can not be bigger than max-value in MinMaxfilter",
                    )
