import os

from search_engine.elastic import Elastic
from singleton import Singleton
from search_engine.arango import ArangoSearch


class SearchManager(metaclass=Singleton):
    def __init__(self):
        self.default_search_engine = os.getenv("SEARCH_ENGINE", "elastic")
        self.search_engines = dict()
        self.__init_default_search_engine()

    def get_search_engine(self):
        return self.search_engines[self.default_search_engine]

    def get_specific_search_engine(self, search_engine):
        if search_engine not in self.search_engines:
            self.__fill_search_engine_store(search_engine)
        return self.search_engines[search_engine]

    def __init_default_search_engine(self):
        self.__fill_search_engine_store(self.default_search_engine)

    def __fill_search_engine_store(self, search_engine):
        if search_engine == "elastic":
            self.search_engines[search_engine] = Elastic()
        elif search_engine == "arango":
            self.search_engines[search_engine] = ArangoSearch()
