import app
import os
import random
import requests

from opensearchpy import OpenSearch

hidden_labels = os.getenv(
    "HIDE_RELATIONS",
    "vervaardiger.rol,techniek,objectnaam,object_category,MensgemaaktObject.draagt,Entiteit.maaktDeelUitVan,MaterieelDing.productie,MensgemaaktObject.maaktDeelUitVan,MaterieelDing.bestaatUit,MaterieelDing.isOvergedragenBijVerwerving,Entiteit.classificatie,Entiteit.wordtNaarVerwezenDoor",
).split(",")
institution_relations = [
    {"key": os.getenv("ARCHIEFGENT_ID"), "value": "Archief Gent"},
    {"key": os.getenv("DMG_ID"), "value": "Design Museum Gent"},
    {"key": os.getenv("HVA_ID"), "value": "Huis van Alijn"},
    {"key": os.getenv("INDUSTRIEMUSEUM_ID"), "value": "Industriemuseum"},
    {"key": os.getenv("SIXTH_COLLECTION_ID"), "value": "Collectie van de Gentenaar"},
    {"key": os.getenv("STAM_ID"), "value": "STAM - Stadsmuseum Gent"},
]
random_relation_limit = os.getenv("RANDOM_RELATION_LIMIT", 10)


def metadata_query(search_value):
    return {
        "query": {
            "nested": {
                "path": "metadata",
                "query": {
                    "bool": {
                        "must": [{"match": {"metadata.value": search_value["value"]}}]
                    }
                },
            }
        },
        "aggs": {
            "relations": {
                "nested": {"path": "relations"},
                "aggs": {
                    "unique_relations": {
                        "terms": {"field": "relations.key", "size": 100},
                        "aggs": {
                            "relations": {
                                "top_hits": {
                                    "size": 1,
                                    "_source": {"includes": ["relations"]},
                                }
                            }
                        },
                    }
                },
            }
        },
    }


def relations_query(search_value):
    return {
        "query": {
            "nested": {
                "path": "metadata",
                "query": {
                    "bool": {
                        "must": [{"match": {"metadata.value": search_value["value"]}}]
                    }
                },
            }
        },
        "aggs": {"unique_vals": {"terms": {"field": "relations.key", "size": 100}}},
        "_source": ["relations"],
    }


def sort_on_query(search_value):
    return {
        "query": {
            "bool": {
                "must": [
                    {"term": {"type": search_value.get("type", "asset")}},
                ]
            },
        },
        "aggs": {
            "relations": {
                "nested": {"path": "relations"},
                "aggs": {
                    "unique_relations": {
                        "terms": {"field": "relations.key", "size": 100},
                        "aggs": {
                            "relations": {
                                "top_hits": {
                                    "size": 1,
                                    "_source": {"includes": ["relations"]},
                                }
                            }
                        },
                    }
                },
            }
        },
    }


def add_fuzzy_search_to_query(query, value):
    match_exact = [
        {"match": {"metadata.key": "height"}},
        {"match": {"metadata.key": "depth"}},
        {"match": {"metadata.key": "width"}},
        {"match": {"metadata.key": "diameter"}},
        {"match": {"metadata.key": "object_number"}},
        {"match": {"metadata.key": "event_time"}},
        {"match": {"metadata.key": "period_start"}},
        {"match": {"metadata.key": "period_end"}},
        {"match": {"metadata.key": "date"}},
    ]
    must_not_fuzzy = [
        {"match": {"metadata.key": "description"}},
        {"match": {"metadata.key": "skos:note"}},
    ]
    must_not_fuzzy.extend(match_exact)
    query["query"]["bool"]["must"].append(
        {
            "nested": {
                "path": "metadata",
                "query": {
                    "bool": {
                        "should": [
                            {
                                "bool": {
                                    "must": [
                                        {
                                            "match": {
                                                "metadata.value": {
                                                    "query": value,
                                                    "fuzziness": 1,
                                                    "max_expansions": 1,
                                                    "operator": "and",
                                                    "prefix_length": 1,
                                                    "boost": 4,
                                                }
                                            }
                                        },
                                        {"match": {"metadata.key": "title"}},
                                    ]
                                }
                            },
                            {
                                "bool": {
                                    "must": [
                                        {
                                            "match": {
                                                "metadata.value": {
                                                    "query": value,
                                                    "fuzziness": 1,
                                                    "max_expansions": 1,
                                                    "operator": "or",
                                                    "prefix_length": 1,
                                                    "boost": 3,
                                                }
                                            }
                                        },
                                        {"match": {"metadata.key": "title"}},
                                    ]
                                }
                            },
                            {
                                "bool": {
                                    "must": [
                                        {
                                            "match": {
                                                "metadata.value": {
                                                    "query": value,
                                                    "fuzziness": 1,
                                                    "max_expansions": 1,
                                                    "operator": "and",
                                                    "prefix_length": 1,
                                                    "boost": 2,
                                                }
                                            }
                                        },
                                        {"bool": {"must_not": must_not_fuzzy}},
                                    ]
                                }
                            },
                            {
                                "bool": {
                                    "must": [
                                        {
                                            "match": {
                                                "metadata.value": {
                                                    "query": value,
                                                    "fuzziness": 1,
                                                    "max_expansions": 1,
                                                    "operator": "or",
                                                    "prefix_length": 1,
                                                }
                                            }
                                        },
                                        {"bool": {"must_not": must_not_fuzzy}},
                                    ]
                                }
                            },
                            {
                                "bool": {
                                    "must": [
                                        {
                                            "match": {
                                                "metadata.value": {
                                                    "query": value.replace(" ", ""),
                                                    "fuzziness": 1,
                                                    "max_expansions": 1,
                                                    "prefix_length": 1,
                                                }
                                            }
                                        },
                                        {"bool": {"must_not": must_not_fuzzy}},
                                    ]
                                }
                            },
                            {
                                "bool": {
                                    "must": [
                                        {
                                            "match": {
                                                "metadata.value": {
                                                    "query": value,
                                                    "operator": "and",
                                                }
                                            }
                                        },
                                        {
                                            "bool": {
                                                "should": [
                                                    {
                                                        "match": {
                                                            "metadata.key": "description"
                                                        }
                                                    },
                                                    {
                                                        "match": {
                                                            "metadata.key": "skos:note"
                                                        }
                                                    },
                                                ]
                                            }
                                        },
                                    ]
                                }
                            },
                            {
                                "bool": {
                                    "must": [
                                        {
                                            "match": {
                                                "metadata.value": {
                                                    "query": value,
                                                }
                                            }
                                        },
                                        {"bool": {"should": match_exact}},
                                    ]
                                }
                            },
                        ]
                    }
                },
            }
        }
    )
    return query


def add_has_mediafile_filter_to_query(query, value):
    query["query"]["bool"]["must"].append({"term": {"has_mediafile": value}})
    return query


def add_randomization_to_query(query, search_value):
    seed = {"seed": search_value["seed"]} if "seed" in search_value else {}
    query["query"] = {"function_score": {"query": query["query"], "random_score": seed}}
    if "sort" in query:
        del query["sort"]
    return query


def add_relations_filter_to_query(query, relation_filters, and_filter=False):
    if and_filter:
        for relation in relation_filters:
            query["query"]["bool"]["must"].append(
                create_nested_query({"term": {"relations.key": relation}})
            )
    else:
        must = [{"terms": {"relations.key": relation_filters}}]
        query["query"]["bool"]["must"].append(create_nested_query(must))
    return query


def get_query(search_value, skip_relations=False):
    ret_query = metadata_query(search_value)
    if "key" in search_value:
        ret_query = sort_on_query(search_value)
        if "has_mediafile" in search_value:
            ret_query = add_has_mediafile_filter_to_query(
                ret_query, search_value["has_mediafile"]
            )
        if search_value["relation_filter"]:
            ret_query = add_relations_filter_to_query(
                ret_query,
                search_value["relation_filter"],
                search_value.get("and_filter", False),
            )
        if search_value["value"] != "":
            ret_query = add_fuzzy_search_to_query(ret_query, search_value["value"])
        if search_value.get("randomize", False):
            ret_query = add_randomization_to_query(ret_query, search_value)
    if skip_relations and "aggs" in ret_query:
        del ret_query["aggs"]
    ret_query["track_total_hits"] = True
    app.logger.info(ret_query)
    return ret_query


def create_nested_query(must_value):
    return {
        "nested": {
            "path": "relations",
            "query": {"bool": {"must": must_value}},
        }
    }


class Elastic:
    def __init__(self):
        self.collection_api_url = os.getenv("COLLECTION_API_URL")
        self.es = OpenSearch([os.getenv("ELASTIC_URL")])
        self.headers = {"Authorization": f'Bearer {os.getenv("STATIC_JWT")}'}

    def __get_output_results(
        self, query_output, limit, skip_relations, auth_token, order_by, ascending
    ):
        ids = [x["_id"] for x in query_output["hits"]["hits"]]
        if not ids:
            return []
        req_url = f'{self.collection_api_url}/entities?ids={",".join(ids)}&limit={limit}&skip_relations={int(skip_relations)}'
        if order_by:
            req_url += f"&order_by={order_by}"
        if ascending:
            req_url += f"&asc={ascending}"
        req = requests.get(
            req_url,
            headers={"Authorization": auth_token} if auth_token else self.headers,
        )
        if req.status_code != 200:
            app.logger.error(f"Failed to get results: {req.content.strip()}")
            return []
        return req.json()["results"]

    def __get_output_relations(self, query_output, relation_limit):
        if "aggregations" not in query_output:
            return []
        all_relations = list()
        for relation in query_output["aggregations"]["relations"]["unique_relations"][
            "buckets"
        ]:
            relation_source = relation["relations"]["hits"]["hits"][0]["_source"]
            label = relation_source.get("label")
            if label and label not in hidden_labels and relation["doc_count"] >= 5:
                extended_relation = {
                    "key": relation["key"],
                    "doc_count": relation["doc_count"],
                    "type": relation_source["type"],
                    "label": label,
                }
                if "value" in relation_source:
                    extended_relation["value"] = relation_source["value"]
                all_relations.append(extended_relation)
        if relation_limit:
            random.shuffle(all_relations)
        relations = list()
        count = 0
        for relation in all_relations:
            if relation["type"] == "isIn" or not relation_limit:
                relations.append(relation)
            elif count < relation_limit:
                relations.append(relation)
                count += 1
        return relations

    def __return_output(
        self,
        output_type,
        query_output,
        query,
        skip,
        limit,
        relation_limit=None,
        skip_relations=False,
        auth_token=None,
        order_by=None,
        ascending=None,
    ):
        if output_type != "collection":
            return query_output
        count = query_output["hits"]["total"]["value"]
        api_output = {
            "count": count,
            "results": self.__get_output_results(
                query_output, limit, skip_relations, auth_token, order_by, ascending
            ),
            "relations": self.__get_output_relations(query_output, relation_limit),
            "limit": limit,
        }
        if skip + limit < count:
            api_output[
                "next"
            ] = f"/search/collection?query={query}&skip={skip + limit}&limit={limit}"
        if skip > 0:
            api_output[
                "previous"
            ] = f"/search/collection?query={query}&skip={max(0, skip - limit)}&limit={limit}"
        return api_output

    def __search_randomized(self, output_type, body, skip, limit, auth_token):
        merged_results = []
        for relation in institution_relations:
            body["relation_filter"] = [relation["key"]]
            partial_limit = int(limit / len(institution_relations))
            partial_skip = int(limit / len(institution_relations))
            query_output = self.es.search(
                body=get_query(body, skip_relations=True),
                params={"size": partial_limit, "from": partial_skip},
            )
            merged_results.extend(
                self.__return_output(
                    output_type,
                    query_output,
                    body,
                    partial_skip,
                    partial_limit,
                    auth_token=auth_token,
                )["results"]
            )
        body["relation_filter"] = [x["key"] for x in institution_relations]
        query_output = self.es.search(
            body=get_query(body), params={"size": limit, "from": skip}
        )
        output = self.__return_output(
            output_type,
            query_output,
            body,
            skip,
            limit,
            relation_limit=random_relation_limit,
            auth_token=auth_token,
        )
        random.shuffle(merged_results)
        output["results"] = merged_results
        for relation in institution_relations:
            if not any(
                x["key"] == relation["key"] and x["type"] == "isIn"
                for x in output["relations"]
            ):
                output["relations"].insert(
                    5,
                    {"type": "isIn", "label": "MaterieelDing.beheerder"} | relation,
                )
        return output

    def check_health(self):
        if not self.es.ping():
            raise Exception("Failed to connect to OpenSearch")
        if self.es.cluster.health()["status"] == "red":
            raise Exception("OpenSearch cluster health is red")
        return "healthy"

    def search(
        self,
        output_type,
        body,
        skip,
        limit,
        auth_token=None,
        oder_by=None,
        ascending=None,
    ):
        if body.get("randomize", False):
            return self.__search_randomized(output_type, body, skip, limit, auth_token)
        skip_relations = body.get("skip_relations", False)
        query_output = self.es.search(
            body=get_query(body, skip_relations=skip_relations),
            params={"size": limit, "from": skip},
        )
        return self.__return_output(
            output_type,
            query_output,
            body,
            skip,
            limit,
            skip_relations=skip_relations,
            auth_token=auth_token,
            oder_by=order_by,
            ascending=ascending,
        )

    def search_relations(self, body):
        return self.es.search(body=relations_query(body), params={})
