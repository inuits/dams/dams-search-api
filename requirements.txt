aniso8601==9.0.1
Authlib==1.2.1
blinker==1.6.2
certifi==2023.5.7
cffi==1.15.1
charset-normalizer==3.1.0
click==8.1.3
cryptography==41.0.1
DateTime==5.1
Flask==2.3.2
Flask-RESTful==0.3.10
flask-swagger-ui==4.11.1
future==0.18.3
gunicorn==20.1.0
idna==3.4
inuits-jwt-auth==2.0.2
itsdangerous==2.1.2
Jinja2==3.1.2
MarkupSafe==2.1.3
opensearch-py==2.2.0
py-healthcheck==1.10.1
pyArango==2.0.2
pycparser==2.21
python-dateutil==2.8.2
pytz==2023.3
requests==2.31.0
sentry-sdk[flask]==0.10.2
six==1.16.0
urllib3==1.26.15
Werkzeug==2.3.6
zope.interface==6.0
