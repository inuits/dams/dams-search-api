#!/bin/bash
DOCKER=docker
if [ -x "$(command -v podman)" ]; then
  DOCKER=podman
fi

${DOCKER} run -it --rm -p 8002:8002 --env APP_ENV=production inuits-dams-search-api:latest $@
